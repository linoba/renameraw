#!/bin/bash

# File should rename files with
# - prefix: IMG_
# - suffix: all
# existing files are overwritten (if new name equals filename of existing file)
# renaming is done in directory and all subdirectories

prefix="LD3A"
echo "prefix is: ${prefix}"

# Replacing Spaces in directory- and filename with underscore
find . -depth -name "* *" -execdir rename 's/ /_/g' "{}" \;

# for files in directory and subdirectories
echo "Renaming following files: "                       # Output
for i in $(find . -name "$prefix*"); do echo "$i" >> renaming_2015_11_17L.txt; done # Output
for i in $(find . -name "$prefix*"); 
do 
    exiftool '-FileName<CreateDate' -d %Y%m%d_%H%M_%%f.%%e "$i" 
done

for i in $(find . -regextype "posix-extended" -regex '.*([A-Za-z0-9_\-]){14}(LD3A){1}.*'); 
do
    rename 's/LD3A//' "$i";
done
echo "End of renaming."

# TOOD use $prefix in regex ?
# TODO use $? as filepath for output
